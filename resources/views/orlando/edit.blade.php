<html><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Payment Control System</title>
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/icon.png">

    <!-- Common Plugins -->
    <link href="{{asset("assets/lib/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- Vector Map Css-->
{{--<link href="{{asset("assets/lib/vectormap/jquery-jvectormap-2.0.2.css")}}" rel="stylesheet"/>--}}

<!-- Chart C3 -->
    <link href="{{asset("assets/lib/chart-c3/c3.min.css")}}" rel="stylesheet">
    <link href="{{asset("assets/lib/chartjs/chartjs-sass-default.css")}}" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">--}}
    {{--<link href="{{asset("assets/lib/datatables/jquery.dataTables.min.css")}}" rel="stylesheet" type="text/css">--}}
    {{--<link href="{{asset("assets/lib/datatables/responsive.bootstrap.min.css")}}" rel="stylesheet" type="text/css">--}}
    <link href="{{asset("assets/lib/toast/jquery.toast.min.css")}}" rel="stylesheet">

    <!-- Custom Css-->
    <link href="{{asset("assets/css/style.css")}}" rel="stylesheet">
    {{--    <link href="http://www.zainkhalid.website/assets/lib/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('client/css/dataTables.colResize.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/datatables.css')}}">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/preview/searchPane/dataTables.searchPane.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css" />




    <style>
        @media print {
            #printPageButton {
                display: none !important;
            }
        }
    </style>

</head>
<body>
@include('layouts.header')
@include('layouts.sidebar')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">

    <div id="signupbox" style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Edit Record</div>
            </div>
            <div class="panel-body">
                <form method="post" action="">
                    @csrf


                    <div id="div_id_username" class="form-group required">
                        <label for="id_username" class="control-label col-md-4  requiredField"> Client Name<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md  textinput textInput form-control" id="client_name" maxlength="90" value="" name="client_name"  style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div id="div_id_email" class="form-group required">
                        <label for="id_email" class="control-label col-md-4  requiredField"> Date<span class="asteriskField">*</span></label>
                        <div class="controls col-md-8 ">
                            <input class="input-md date form-control" id="date" name="date" value="" style="margin-bottom: 10px" type="date">
                        </div>
                    </div>
                    <div id="div_id_password1" class="form-group required">
                        <label for="id_password1" class="control-label col-md-4  requiredField">Type<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="type" name="type" value="" style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div id="div_id_password2" class="form-group required">
                        <label for="id_password2" class="control-label col-md-4  requiredField"> Total <span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="id_password2" name="password2" placeholder="Confirm your password" style="margin-bottom: 10px" type="password">
                        </div>
                    </div>
                    <div id="div_id_name" class="form-group required">
                        <label for="id_name" class="control-label col-md-4  requiredField"> full name<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="id_name" name="name" placeholder="Your Frist name and Last name" style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div id="div_id_gender" class="form-group required">
                        <label for="id_gender" class="control-label col-md-4  requiredField"> Gender<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 " style="margin-bottom: 10px">
                            <label class="radio-inline"> <input type="radio" name="gender" id="id_gender_1" value="M" style="margin-bottom: 10px">Male</label>
                            <label class="radio-inline"> <input type="radio" name="gender" id="id_gender_2" value="F" style="margin-bottom: 10px">Female </label>
                        </div>
                    </div>
                    <div id="div_id_company" class="form-group required">
                        <label for="id_company" class="control-label col-md-4  requiredField"> company name<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="id_company" name="company" placeholder="your company name" style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div id="div_id_catagory" class="form-group required">
                        <label for="id_catagory" class="control-label col-md-4  requiredField"> catagory<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="id_catagory" name="catagory" placeholder="skills catagory" style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div id="div_id_number" class="form-group required">
                        <label for="id_number" class="control-label col-md-4  requiredField"> contact number<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="id_number" name="number" placeholder="provide your number" style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div id="div_id_location" class="form-group required">
                        <label for="id_location" class="control-label col-md-4  requiredField"> Your Location<span class="asteriskField">*</span> </label>
                        <div class="controls col-md-8 ">
                            <input class="input-md textinput textInput form-control" id="id_location" name="location" placeholder="Your Pincode and City" style="margin-bottom: 10px" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls col-md-offset-4 col-md-8 ">
                            <div id="div_id_terms" class="checkbox required">
                                <label for="id_terms" class=" requiredField">
                                    <input class="input-ms checkboxinput" id="id_terms" name="terms" style="margin-bottom: 10px" type="checkbox">
                                    Agree with the terms and conditions
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="aab controls col-md-4 "></div>
                        <div class="controls col-md-8 ">
                            <input type="submit" name="Signup" value="Signup" class="btn btn-primary btn btn-info" id="submit-id-signup">
                            or <input type="button" name="Signup" value="Sign Up with Facebook" class="btn btn btn-primary" id="button-id-signup">
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>




<!-- Common Plugins -->
<script src="{{asset("assets/lib/jquery/dist/jquery.min.js")}}"></script>
<script src="{{asset("assets/lib/bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("assets/lib/bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("assets/lib/pace/pace.min.js")}}"></script>
<script src="{{asset("assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js")}}"></script>
<script src="{{asset("assets/lib/slimscroll/jquery.slimscroll.min.js")}}"></script>
<script src="{{asset("assets/lib/nano-scroll/jquery.nanoscroller.min.js")}}"></script>
<script src="{{asset("assets/lib/metisMenu/metisMenu.min.js")}}"></script>
<script src="{{asset("assets/js/custom.js")}}"></script>

<!--Chart Script-->
<script src="{{asset("assets/lib/chartjs/chart.min.js")}}"></script>
<script src="{{asset("assets/lib/chartjs/chartjs-sass.js")}}"></script>

<script src="{{asset("assets/lib/chart-c3/d3.min.js")}}"></script>
<script src="{{asset("assets/lib/chart-c3/c3.min.js")}}"></script>

<script src="{{asset("assets/lib/toast/jquery.toast.min.js")}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="{{asset('client/js/jQuery.js')}}"></script>
<script src="{{asset('client/js/jquery-ui.js')}}"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{asset('client/js/dropzone.js')}}"></script>

<script charset="utf8" src="{{asset('client/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('client/js/dataTables.colResize.js')}}"></script>
<script src="{{asset('client/app.js')}}"></script>
<script src="{{asset('client/js/examples.datatables.editable.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.forPrice').keypress(function (eve) {
        if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0)) {
            eve.preventDefault();
        }
    });
    $('.forPrice').keyup(function (eve) {
        if ($(this).val().indexOf('.') == 0) {
            $(this).val($(this).val().substring(1));
        }
    });

</script>
<script src="{{asset('client/js/examples.datatables.editable.js')}}"></script>
<script src="{{asset('client/app.js')}}"></script>



</body></html>
