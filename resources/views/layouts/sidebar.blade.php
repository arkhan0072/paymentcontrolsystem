
<div class="main-sidebar-nav dark-navigation">
    <div class="nano has-scrollbar">
        <div class="nano-content sidebar-nav" tabindex="0" style="right: -17px;">

            <div class="card-body border-bottom text-center nav-profile">
                <div class="notify setpos"><span class="heartbit"></span> <span class="point"></span></div>
                <img alt="profile" class="margin-b-10  " src="{{asset("assets/img/avtar-5.png")}}" width="80">
                {{--<p class="lead margin-b-0 toggle-none"> {{\Auth::user()->name}}--}}

                    {{--@if(\Auth::user()->hasRole('admin'))--}}
                        {{--<b>(Admin)</b>--}}
                    {{--@elseif(\Auth::user()->hasRole('manager'))--}}
                        {{--<b>(Manager)</b>--}}
                    {{--@elseif(\Auth::user()->hasRole('customer'))--}}
                        {{--<b>(Customer)</b>--}}
                    {{--@endif--}}

                {{--</p>--}}
                <p class="text-muted mv-0 toggle-none">Welcome</p>
            </div>

            <ul class="metisMenu nav flex-column" id="menu">

                <li class="nav-heading"><span>MAIN</span></li>

                <li class="nav-item active"><a class="nav-link" href="{{ route('home')}}"><i class="fa fa-home"></i>
                        <span class="toggle-none">Home </span></a></li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-map-o"></i>
                        <span class="toggle-none">Logs <span class="fa arrow"></span></span></a>
                    <ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">
                        <li class="nav-item"><a class="nav-link" href="{{route('logs')}}">View All Import Files</a>
                        </li>
{{--                        <li class="nav-item"><a class="nav-link" href="{{route('location.index')}}">All Locations</a>--}}
{{--                        </li>--}}
                    </ul>
                </li>


                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-list"></i>--}}
                        {{--<span class="toggle-none"> Categories <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('category.create')}}">New Category</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('category.index')}}">All Categories</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}



                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-cutlery"></i>--}}
                        {{--<span class="toggle-none"> Products <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('product.create')}}">New Product</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('product.index')}}">All Products</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-male"></i>--}}
                        {{--<span class="toggle-none"> Suppliers <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('supplier.create')}}">New Supplier</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('supplier.index')}}">All Suppliers</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-magic"></i>--}}
                        {{--<span class="toggle-none"> Inventory <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('inventory.create')}}">New Inventroy</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('inventory.index')}}">Inventory <Index></Index></a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('inventory.check')}}">Check Invetroy</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}






                {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-map-o"></i>--}}
                {{--<span class="toggle-none">Template <span class="fa arrow"></span></span></a>--}}
                {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('template.create')}}">New Template</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('template.index')}}">All Template</a>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--</li>--}}

                {{--////////plot/////--}}
                {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                {{--class="fa fa-map-marker"></i> <span class="toggle-none">Plot <span--}}
                {{--class="fa arrow"></span></span></a>--}}
                {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('plot.create')}}">New Plot</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('plot.index')}}">All Plots</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}

                {{--////////sales ///--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-shopping-cart"></i> <span class="toggle-none">Sales <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('sale.create')}}">New Sale</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('sale.index')}}">All Sales</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('sale.canceled')}}">All Canceled Sale</a></li>--}}

                    {{--</ul>--}}
                {{--</li>--}}

                {{--///////Payments--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-money"></i>--}}
                        {{--<span class="toggle-none">Payments <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.create')}}">New Payment</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.index')}}">All Payments</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-map-o"></i>--}}
                        {{--<span class="toggle-none">Account <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('account.create')}}">New Account</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('account.index')}}">All Accounts</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--////////expense /--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-dollar"></i> <span class="toggle-none">Transaction <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('expense.create')}}">New Transaction</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('transaction.index')}}">All--}}
                                {{--Transaction</a></li>--}}
                    {{--</ul>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('transaction.cash')}}">All--}}
                                {{--Cash In Hand</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                {{--class="fa fa-dollar"></i> <span class="toggle-none">Transaction Details <span--}}
                {{--class="fa arrow"></span></span></a>--}}
                {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('transaction.index')}}">All Transaction</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}

                {{--////////Calls ///--}}
                {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                {{--class="fa fa-phone"></i> <span class="toggle-none">Installment Calls<span--}}
                {{--class="fa arrow"></span></span></a>--}}
                {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.currentdate')}}">Upcom-Installments</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.perivousdate')}}">Prev-Instalments</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item"><a class="nav-link"--}}
                {{--href="{{route('payment.advancedues')}}">Upcom-Advance</a></li>--}}

                {{--</ul>--}}

                {{--</li>--}}

                {{--///////Manager--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-id-card-o"></i> <span class="toggle-none">Managers <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column  collapse" aria-expanded="false">--}}

                        {{--<li class="nav-item"><a class="nav-link" href="{{route('manager.create')}}">New Manager</a></li>--}}

                        {{--<li class="nav-item"><a class="nav-link" href="{{route('manager.index')}}">All Managers</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--///////Setting--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa icon-settings"></i> <span class="toggle-none">Setting <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column  collapse" aria-expanded="false">--}}

                        {{--<li class="nav-item"><a class="nav-link" href="{{route('change.setting')}}">Change Setting</a>--}}
                        {{--</li>--}}


                    {{--</ul>--}}
                {{--</li>--}}
                {{--@endrole--}}


                {{--@role('manager')--}}

                {{--////////plot/////--}}

                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-map-o"></i>--}}
                        {{--<span class="toggle-none">Accounts <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('account.create')}}">New Account</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('account.index')}}">All Accounts</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-map-o"></i>--}}
                        {{--<span class="toggle-none">Template <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('template.create')}}">New Template</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('template.index')}}">All Template</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-map-marker"></i> <span class="toggle-none">Plots <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('plot.create')}}">New Plot</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('plot.index')}}">All Plots</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--////////sales ///--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-shopping-cart"></i> <span class="toggle-none">Sales <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('sale.create')}}">New Sale</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('sale.index')}}">All Sales</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('sale.canceled')}}">All Canceled Sale</a></li>--}}

                    {{--</ul>--}}
                {{--</li>--}}
                {{--////////expense /--}}

                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-dollar"></i> <span class="toggle-none">Transaction <span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('expense.create')}}">New Transaction</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('transaction.index')}}">All--}}
                                {{--Transaction</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                {{--class="fa fa-dollar"></i> <span class="toggle-none">Transaction Details <span--}}
                {{--class="fa arrow"></span></span></a>--}}
                {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                {{--<li class="nav-item"><a class="nav-link" href="{{route('transaction.index')}}">All Transaction</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i--}}
                                {{--class="fa fa-phone"></i> <span class="toggle-none">Installment Calls<span--}}
                                    {{--class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.currentdate')}}">Upcom-Installments</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.perivousdate')}}">Prev-Instalments</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a class="nav-link"--}}
                                                {{--href="{{route('payment.advancedues')}}">Upcom-Advance</a></li>--}}

                    {{--</ul>--}}

                {{--</li>--}}
                {{--///////Payments--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-money"></i>--}}
                        {{--<span class="toggle-none">Payments <span class="fa arrow"></span></span></a>--}}
                    {{--<ul class="nav-second-level nav flex-column sub-menu collapse" aria-expanded="false">--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.create')}}">New Payment</a></li>--}}
                        {{--<li class="nav-item"><a class="nav-link" href="{{route('payment.index')}}">All Payments</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}


                {{--@endrole--}}

            {{--</ul>--}}
        </div>
        <div class="nano-pane">
            <div class="nano-slider" style="height: 71px; transform: translate(0px, 0px);"></div>
        </div>
    </div>
</div>
