<div class="top-bar light-top-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a class="admin-logo dark-logo" href="index.html">
                    <h1 style="font-size: 16px;">
                       PaymentControl
                    </h1>
                </a>
                <div class="left-nav-toggle" >
                    <a  href="#" class="nav-collapse"><i class="fa fa-bars"></i></a>
                </div>
                <div class="left-nav-collapsed" >
                    <a  href="#" class="nav-collapsed"><i class="fa fa-bars"></i></a>
                </div>
                <div class="search-form hidden-xs">
                    {{--<li class="nav-item d-none d-sm-inline-block">--}}
                        {{--@if(request()->session()->has('selected_location'))--}}
                            {{--<a href="{{route('home')}}" class="btn btn-outline-danger">Selected Location : {{\Session::get('selected_location.name')}}</a>--}}
                        {{--@else--}}
                            {{--<a href="{{route('home')}}" class="btn btn-outline-danger">Location Not Selected</a>--}}
                        {{--@endif--}}
                    {{--</li>--}}
                    {{--<form>--}}
                        {{--<input class="form-control" placeholder="Search for..." type="text"> <button class="btn-search" type="button"><i class="fa fa-search"></i></button>--}}
                    {{--</form>--}}
                </div>
                <ul class="list-inline top-right-nav">
                    {{--<li class="dropdown icons-dropdown d-none-m">--}}
                        {{--<a class="dropdown-toggle " data-toggle="dropdown" href="#"><i class="fa fa-envelope"></i> <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div></a>--}}

                        {{--<ul class="dropdown-menu top-dropdown lg-dropdown notification-dropdown">--}}
                            {{--<li>--}}
                                {{--<div class="dropdown-header">--}}
                                    {{--<a class="float-right" href="#"><small>View All</small></a> Messages--}}
                                {{--</div>--}}

                                {{--<div class="scrollDiv">--}}
                                    {{--<div class="notification-list">--}}
                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
														{{--<img alt="" class="rounded-circle" src="{{asset("assets/img/avtar-2.png")}}" width="50">--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">--}}
													{{--John Doe--}}
													{{--<label class="label label-warning float-right">Support</label>--}}
													{{--</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
													{{--<img alt="" class="rounded-circle" src="{{asset("assets/img/avtar-3.png")}}" width="50">--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">--}}
													{{--Govindo Doe--}}
													{{--<label class="label label-warning float-right">Support</label>--}}
													{{--</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
													{{--<img alt="" class="rounded-circle" src="assets/img/avtar-4.png" width="50">--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">--}}
													{{--Megan Doe--}}
													{{--<label class="label label-warning float-right">Support</label>--}}
													{{--</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
													{{--<img alt="" class="rounded-circle" src="assets/img/avtar-5.png" width="50">--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">--}}
													{{--Hritik Doe--}}
													{{--<label class="label label-warning float-right">Support</label>--}}
													{{--</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown icons-dropdown d-none-m">--}}
                        {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-bell"></i> <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div></a>--}}
                        {{--<ul class="dropdown-menu top-dropdown lg-dropdown notification-dropdown">--}}
                            {{--<li>--}}
                                {{--<div class="dropdown-header">--}}
                                    {{--<a class="float-right" href="#"><small>View All</small></a> Notifications--}}
                                {{--</div>--}}
                                {{--<div class="scrollDiv">--}}
                                    {{--<div class="notification-list">--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
														{{--<i class="icon-cloud-upload text-primary"></i>--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">Upload Complete</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
														{{--<i class="icon-info text-warning"></i>--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">Storage Space low</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
														{{--<i class="icon-check text-success"></i>--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">Project Task Complete</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                        {{--<a class="clearfix" href="javascript:%20void(0);">--}}
													{{--<span class="notification-icon">--}}
														{{--<i class=" icon-graph text-danger"></i>--}}
													{{--</span>--}}
                                            {{--<span class="notification-title">CPU Usage</span>--}}
                                            {{--<span class="notification-description">Lorem Ipsum is simply dummy text of the printing.</span>--}}
                                            {{--<span class="notification-time">15 minutes ago</span>--}}
                                        {{--</a>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a class="right-sidebar-toggle d-none-m" href="javascript:%20void(0);"><i class="fa fa-align-right"></i></a>--}}
                    {{--</li>--}}
                    <li class="dropdown avtar-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" class="rounded-circle" src="{{asset("assets/img/avtar-5.png")}}" width="30">
                            {{--{{\Illuminate\Support\Facades\Auth::user()->name}}--}}
                        </a>
                        <ul class="dropdown-menu top-dropdown">
                            <li>
                                <a class="dropdown-item" href="javascript:%20void(0);"><i class="icon-bell"></i> Activities</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript:%20void(0);"><i class="icon-user"></i> Profile</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript:%20void(0);"><i class="icon-settings"></i> Settings</a>
                            </li>
                            <li class="dropdown-divider"></li>
                            {{--<li>--}}
                                {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                                   {{--onclick="event.preventDefault();--}}
                            {{--document.getElementById('logout-form').submit();">--}}
                                    {{--<i class="icon-logout"></i>--}}
                                    {{--{{ __('Logout') }}--}}
                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                        {{--@csrf--}}
                                    {{--</form>--}}

                                {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
