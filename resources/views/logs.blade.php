@extends('layouts.master')
@section('title')
    <section class="content-header" style="margin-top: 20px;">
        <h1>
            Dashboard
            {{--                <small>Control panel</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Payment Control</a></li>
            <li class="breadcrumb-item active">{{\Route::currentRouteName()}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-white anime">
                        Logs Overview
                        <p class="text-white">Log Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table id="data" class="table table-bordered" style="width:100%">
                            <thead>
                            <tr class="bg-pale-dark">
                                <th>Id</th>
                                <th>File Name</th>
                                <th>Date</th>
                                <th>Create Time</th>
                                <th>Update Time</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {
            $('#data thead tr').clone(true).appendTo('#data thead').attr('id', 'tr2');

            $('#data thead tr:eq(1) th').each(function (i) {
                if (i === $('#data thead tr:eq(1) th').length - 1) {
                    return;
                }
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {

                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });


            var methodes;
            table = $('#data').DataTable({
                dom: 'lBftip',
                buttons: [
                    {
                        text: 'Add',
                        className: 'btn dark btn-outline',
                        action: function (e, dt, node, config) {
                            window.location.href = 'add_quote.php';
                        }
                    },
                    {extend: 'print', className: 'btn dark btn-outline'},
                    {extend: 'copy', className: 'btn red btn-outline'},
                    {extend: 'pdf', className: 'btn green btn-outline'},
                    {extend: 'excel', className: 'btn yellow btn-outline '},
                    {extend: 'csv', className: 'btn purple btn-outline '},
                    {extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}

                ],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: true,
                ajax: {
                    url: '{{route('getlogs')}}',
                },
                columns: [
                    {data: 'id', Title: 'id', orderable: false},
                    {data: 'file_name', Title: 'Client', orderable: false},
                    {data: 'date', Title: 'Date', orderable: false},
                    {data: 'created_at', Title: 'Create Time', orderable: false},
                    {data: 'updated_at', Title: 'Update Time', orderable: false},
                ],
                "columnDefs": [

                    {
                        "targets": 0
                    },

                    {
                        "targets": 1
                    },
                    {

                        "targets": 2
                    },
                    {

                        "targets": 3
                    },
                    {

                        "targets": 4
                    }
                ],
            });
            });




    </script>
@endsection


