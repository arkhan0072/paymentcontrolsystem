@extends('layouts.master')
@section('title')
    <section class="content-header" style="margin-top: 20px;">
        <h1>
            Dashboard
            {{--                <small>Control panel</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Payment Control</a></li>
            <li class="breadcrumb-item active">{{Route::currentRouteName()}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 d-flex">
                    <div class="card col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Last Import</h5>
                            <div class="text-center">
                                <a href="#" style="font-size: 30px!important;"><i class="fa fa-clock-o"></i> <span
                                        id="log"></span></a>
                            </div>
                        </div>
                    </div>

                    <div class="card col-md-7" style="margin-left: 25px;">
                        <div class="card-body">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <h5 class="card-title">Import Csv</h5>
                            <div class="text-center">
                                <form action="{{ route('import') }}" class="form-horizontal d-flex" method="post"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="file" name="file" class="form-control"/>
                                    <button class="btn btn-primary">Import File</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 d-flex">

                    <div class="card float-right" style="margin-left: 25px;">
                        <div class="card-body">
                            <h5 class="card-title">Between</h5>
                            <div class="text-center">
                                <a href="#" style="font-size: 30px!important;"><i class="fa fa-dollar"></i> <span
                                        id="totaluncolleted"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="card float-right" style="margin-left: 25px;">
                        <div class="card-body">
                            <h5 class="card-title">UnCollected Cash</h5>
                            <div class="text-center">
                                <a href="#" style="font-size: 30px!important;"><i class="fa fa-dollar"></i> <span
                                        id="totaluncolletedcash"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="card float-right" style="margin-left: 25px;">
                        <div class="card-body">
                            <h5 class="card-title">UnCollected check</h5>
                            <div class="text-center">
                                <a href="#" style="font-size: 30px!important;"><i class="fa fa-dollar"></i> <span
                                        id="totaluncolletedcheck"></span></a>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-lg-12 col-sm-12  col-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Latest Transactions</h4>
                        </div>
                        <div class="box-body">
                            <div class="d-flex">
                                <div class="form-group">
                                    <label class="title">Payment Methods</label>
                                    <select name="methods" id="methodz" class="form-control">
                                        <option value="">All</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Check">Check</option>
                                        <option value="Credit Card">Credit Card</option>
                                        <option value="Jobber Payments">Jobber Payments</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="title">Payment Status</label>
                                    <select name="payments" id="payments" class="form-control">

                                        <option value="">All</option>
                                        <option value="Y">Collected</option>
                                        <option value="N">Uncollected</option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label class="title"> <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        Select Date Range to filter</label>
                                    <div class="form-group d-flex">
                                        <input type="text" id="datepick" name="datepick" value=""/>
                                        <button class="cancelBtn btn btn-sm btn-default" name="reset" type="button">
                                            Reset
                                        </button>
                                    </div>

                                </div>

                            </div>

                            <div class="table-responsive">
                                <table id="data" class="display" style="width:100%">
                                    <thead>
                                    <tr class="bg-pale-dark">
                                        <th>Edit</th>
                                        <th>Id</th>
                                        <th>Client</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Total</th>
                                        <th>Note</th>
                                        <th>Check</th>
                                        <th>Invoice</th>
                                        <th>Method</th>
                                        <th>Transaction</th>
                                        <th>Employee</th>
                                        <th>Received</th>
                                        <th>Notes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" id="update_trans">
                                        @csrf
                                        <input type="hidden" name="id" id="update_id" value="">
                                        <div id="div_id_username" class="form-group required">
                                            <label for="id_username" class="control-label col-md-4  requiredField">
                                                Client Name<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md  textinput textInput form-control"
                                                       id="client_name" maxlength="90" value="" name="client_name"
                                                       style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_email" class="form-group required">
                                            <label for="id_email" class="control-label col-md-4  requiredField">
                                                Date<span class="asteriskField">*</span></label>
                                            <div class="controls col-md-8 ">
                                                <div class="input-group date" data-provide="datepicker">
                                                    <input type="text" id="date" name="date" value=""
                                                           class="form-control">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="div_id_password1" class="form-group required">
                                            <label for="id_password1" class="control-label col-md-4  requiredField">Type<span
                                                    class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control" id="type"
                                                       name="type" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_password2" class="form-group required">
                                            <label for="id_password2" class="control-label col-md-4  requiredField">
                                                Total <span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control" id="total"
                                                       name="total" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_name" class="form-group required">
                                            <label for="id_name" class="control-label col-md-4  requiredField">
                                                Note<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <textarea class="input-md textinput textInput form-control" id="note"
                                                          name="note" value="" style="margin-bottom: 10px"></textarea>
                                            </div>
                                        </div>
                                        <div id="div_id_name" class="form-group required">
                                            <label for="id_name" class="control-label col-md-4  requiredField">
                                                Check_<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <textarea class="input-md textinput textInput form-control" id="check"
                                                          name="check" value="" style="margin-bottom: 10px"></textarea>
                                            </div>
                                        </div>
                                        <div id="div_id_name" class="form-group required">
                                            <label for="id_name" class="control-label col-md-4  requiredField">
                                                Invoice<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control" id="invoice"
                                                          name="invoice" value=""
                                                          style="margin-bottom: 10px">
                                            </div>
                                        </div>
                                        <div id="div_id_company" class="form-group required">
                                            <label for="id_company" class="control-label col-md-4  requiredField">
                                                Method<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control" id="method"
                                                       name="methods" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_catagory" class="form-group required">
                                            <label for="id_catagory" class="control-label col-md-4  requiredField">
                                                Transaction<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control"
                                                       id="transaction" name="transaction" value=""
                                                       style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_catagory" class="form-group required">
                                            <label for="id_catagory" class="control-label col-md-4  requiredField">
                                                Employee<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control" id="employee"
                                                       name="employee" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_location" class="form-group required">
                                            <label for="id_location" class="control-label col-md-4  requiredField">
                                                Received<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <input class="input-md textinput textInput form-control" id="received"
                                                       name="received" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_location" class="form-group required">
                                            <label for="id_location" class="control-label col-md-4  requiredField">
                                                Notes<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-8 ">
                                                <textarea class="input-md textinput textInput form-control" id="notes"
                                                          name="notes" value="" style="margin-bottom: 10px"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default close" data-dismiss="modal">
                                                Close
                                            </button>
                                            <button type="submit" class="btn btn-default update-trans">Update</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>




@endsection

@section('script')
    <script>

        var table;
        var between;
        var id;
        $(document).ready(function () {

            $('#data thead tr').clone(true).appendTo('#data thead').attr('id', 'tr2');

            $('#data thead tr:eq(1) th').each(function (i) {
                if (i === $('#data thead tr:eq(1) th').length - 1) {
                    return;
                }
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {

                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });


            var methodes;
            table = $('#data').DataTable({

                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: true,

                ajax: {
                    url: '{{route('paymentdata')}}',
                    data: function (d) {
                        d.methodz = $('#methodz :selected').val();
                        d.payments = $('#payments :selected').val();
                        d.datepick = $('#datepick').val();

                    }
                },
                columns: [
                    {data: 'edit', Title: 'edit', orderable: false},
                    {data: 'id', Title: 'id', orderable: false},
                    {data: 'client_name', Title: 'Client', orderable: false},
                    {data: 'date', Title: 'Date', orderable: false},
                    {data: 'type', Title: 'Type', orderable: false},
                    {data: 'total', Title: 'Total', orderable: false},
                    {data: 'note', Title: 'Note', orderable: false},
                    {data: 'check_', Title: 'Check', orderable: false},
                    {data: 'invoice', Title: 'Invoice', orderable: false},
                    {data: 'method', Title: 'Method', orderable: false},
                    {data: 'transaction_id', Title: 'Transaction', orderable: false},
                    {data: 'employee', Title: 'Employee', orderable: false},
                    {data: 'received', Title: 'Received', orderable: false},
                    {data: 'notes', Title: 'Notes', orderable: false},
                ],
                buttons: [

                    {extend: 'print', className: 'btn dark btn-outline'},
                    {extend: 'copy', className: 'btn red btn-outline'},
                    {extend: 'pdf', className: 'btn green btn-outline'},
                    {extend: 'excel', className: 'btn yellow btn-outline '},
                    {extend: 'csv', className: 'btn purple btn-outline '},
                    {extend: 'colvis', className: 'btn btn-primary', text: 'Columns'}
                ],
                columnDefs: [

                    {
                        "width": "3px",
                        "targets": 0
                    },

                    {
                        "width": "43px",
                        "targets": 1
                    },
                    {
                        "width": "50px",
                        "targets": 2
                    },
                    {
                        "width": "45px",
                        "targets": 3,
                    },
                    {
                        "width": "37px",
                        "targets": 4
                    },
                    {
                        "width": "25px",
                        "targets": 5
                    },
                    {
                        "width": "38px",
                        "targets": 6
                    },
                    {
                        "width": "34px",
                        "targets": 7
                    },
                    {
                        "width": "36px",
                        "targets": 8
                    },
                    {
                        "width": "36px",
                        "targets": 9
                    },
                    {
                        "width": "15px",
                        "targets": 10
                    },
                    {
                        "width": "35px",
                        "targets": 11
                    },
                    {
                        "width": "120px",
                        "targets": 12
                    }],
            });


            $('#methodz').on('change', function () {
                table.draw();
            });
            $('#payments').on('change', function () {
                table.draw();
            });
            table.on('xhr', function () {
                var json = table.ajax.json();
                $('#totaluncolleted').html('Uncollected Between ' + json.totaluncolletedbetween),
                    $('#totaluncolleted').html('Collected Between ' + json.totaluncolletedbetween),
                    $('#totaluncolletedcash').html(json.totaluncolletedCash),
                    $('#totaluncolletedcheck').html(json.totaluncolletedcheck),
                    $('#log').html(json.logs);
            });
            /* Predefined Ranges */
            //
            // var start = moment().subtract('90','days');
            // var end = moment();

            function cb(start, end) {
                between = $('#datepick').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));

                table.draw();
            }


            // cb(start, end)


            $(function () {
                var start, end;
                $('input[name="datepick"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });
                $('input[name="datepick"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                    table.draw();
                });

                $('input[name="datepick"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                    table.draw();
                });
                //
                // $('#datepick').daterangepicker({
                //     startDate: start,
                //     endDate: end,
                //     ranges: {
                //         'Today': [moment(), moment()],
                //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                //         'This Month': [moment().startOf('month'), moment().endOf('month')],
                //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                //     }
                // },cb);

            });
            $('.cancelBtn').on('click', function () {
                location.reload();
            });


        });

        window.addEventListener("load", function () {
            setTimeout(function () {
                $('.btn-primary').on('click', function () {
                    id = $(this).data('id');
                    $.ajax({
                        type: "get",
                        url: '{{route('editOrlando')}}',
                        data: {
                            id: id
                        },
                        success: function (response) {
                            $('#update_id').val(response.id);
                            $('#client_name').val(response.client_name);
                            $('.datepicker').datepicker({
                                format: 'mm/dd/yyyy',
                            });
                            $('#date').val(response.date);
                            $('#type').val(response.type);
                            $('#total').val(response.total);
                            $('#note').val(response.note);
                            $('#check').val(response.check_);
                            $('#invoice').val(response.invoice);
                            $('#method').val(response.method);
                            $('#transaction').val(response.transaction_id);
                            $('#employee').val(response.employee);
                            $('#received').val(response.received);
                            $('#notes').val(response.notes);
                            console.log(response);
                        }

                    });

                });
            }, 5000);

        }, false);
        $('.update-trans').on('click', function (e) {
            e.preventDefault();

            var datas = $("#update_trans").serialize();
            $.ajax({
                type: "POST",
                url: '{{route('updateOrlando')}}',
                data: {
                    id: $('#update_id').val(),
                    client_name: $('#client_name').val(),
                    date: $('#date').val(),
                    type: $('#type').val(),
                    total: $('#total').val(),
                    note: $('#note').val(),
                    check: $('#check').val(),
                    invoice: $('#invoice').val(),
                    methods: $('#method').val(),
                    transaction: $('#transaction').val(),
                    employee: $('#employee').val(),
                    received: $('#received').val(),
                    notes: $('#notes').val(),
                },
                success: function (response) {
                    console.log(response)
                    table.draw();
                    $('.close').click();
                }

            })
        });
        /*$('.applyBtn').on('click',function(){

            // table.destroy();
            table.draw(); // Redraw the DataTable
        });*/


    </script>
@endsection


