/**
 * Created by trimmytech on 25/02/2019.
 */
/**
 * Created by minfo on 31/08/2018.
 */

var maxDateFilter = "";
var minDateFilter = "";

var dateFormat = "mm/dd/yy",
    from = $( "#from" )
        .datepicker()
        .on( "change", function() {
            var mDate = getDate( this );
            to.datepicker( "option", "minDate",  mDate);
            submitForm1($("#analytic-form-1"), $('#uc-cash-label'));
            minDateFilter = formatDate(mDate);
            table.draw();
        }),
    to = $( "#to" ).datepicker()
        .on( "change", function() {
            var mDate = getDate( this );
            from.datepicker( "option", "maxDate",  mDate);
            submitForm1($("#analytic-form-1"), $('#uc-cash-label'));
            maxDateFilter = formatDate(mDate);
            table.draw();
        });

function formatDate(mDate){
    var DateInstance = new Date(mDate);
    return (DateInstance.getMonth() + 1) + '/' + DateInstance.getDate() + '/' + DateInstance.getFullYear();
}

function getDate( element ) {
    var date;
    try {
        date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
        date = null;
    }

    return date;
}


$('#from, #to').change(function () {
    $('#from-id').text($("#from").val());
    //
    if($("#to").val() != '') {
        $('#and-id').text(" and ");
        //
        $('#to-id').text($("#to").val());
    }
    
    table.draw();
});


$('#payment-select, #received-select').change(function () {
    if($(this).hasClass("payment")){
        $('#type-payment-id').text($(this).val());
    }else {
        var lab = $(this).val() == "Y"? "Collected" : ($(this).val() == "N" ? "UnCollected" : "UnCollected");
        $('#collected-lab-id').text(lab);
    }
    submitForm1($("#analytic-form-1"), $('#uc-cash-label'));
    table.draw();
});

function submitForm1($form, $resultLabel){
    var url = $form.attr('action');
    var data = $form.serialize();

    $.ajax({
        type: "POST",
        url: url,
        data: $form.serialize(), // serializes the form's elements.
        success: function(data)
        {
            console.log("URL : "+url+" . Data: "+data);
            $resultLabel.html("$ " + data);
        }
    });
}


// Initialize DropZone
Dropzone.options.import = {
    paramName: "transactions-file",
    maxFilesize: 20, // MB
    accept: function(file, done) {
        done();
    },

    success: function(){
        var args = Array.prototype.slice.call(arguments);
        location.reload();
        //console.log(args);
    },

    error: function(){
        var args = Array.prototype.slice.call(arguments);
        //console.log(args);
    }
};

var table;

$(document).ready(function() {

submitForm1($("#analytic-form-1"), $('#uc-cash-label'));
            //minDateFilter = formatDate(mDate);
            //table.draw();

    $('#transactions thead tr').clone(true).appendTo( '#transactions thead' );
    $('#transactions thead tr:eq(1) th').each( function (i) {
        $(this).html('');
        if(i == 1 || i == 2 || i == 8 || i == 10 || i == 11){
            var title = $(this).text();
            if(i == 11){
                $(this).html( '<select><option value=""></option><option value="Y">Y</option><option value="N">N</option></select>');
                $( 'select', this ).on( 'change', function () {
                    if ( table.column(i).search() !== this.value && this.value !== '') {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                });

            }else {
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                });
            }
        }
    });

    // Initialize DT
    table = $('#transactions').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "type": "post",
            "url": "server/transactions-api.php",
            "data": function (d) {
                console.log($('#payment-select').val());
                d.fromDate = minDateFilter;
                d.toDate = maxDateFilter;
                d.payType = $('#payment-select').val();
                d.collected = $('#received-select').val();
            }
        },
        "ordering": true,
        "orderCellsTop": true,
        'colResize': true,
        'autoWidth': false,
        'pageLength': 25,
        'searching': true,
        'columns': [
            {"data":null, "className": "action_ed_col",
                "defaultContent":['<a href="#" class="on-default edit-row fa-2x"><i class="fa fa-pencil"></i></a>',
                    '<a href="#" class="hidden on-editing save-row fa-2x"><i class="fa fa-save"></i></a>',
                    '<a href="#" class="hidden on-editing cancel-row fa-2x"><i class="fa fa-times"></i></a>'
                ].join(' ')
            },
            { "data": 0 },
            { "data": 1 },
            { "data": 2 },
            { "data": 3 },
            { "data": 4 },
            { "data": 5 },
            { "data": 6 },
            { "data": 7 },
            { "data": 8 },
            { "data": 9 },
            { "data": 10, "className": "rec_col"},
            { "data": 11 }
        ]
    } );

    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
        var column = table.column( $(this).attr('data-column') );
        column.visible( ! column.visible() );
    } );

    $('a.toggle-all').on('click', function(e){
        table.columns().visible(true);
    });

    $('button.reset').on('click', function(){
        window.location.replace("http://rec.minthousecleaning.com/orlando/");
    });
} );