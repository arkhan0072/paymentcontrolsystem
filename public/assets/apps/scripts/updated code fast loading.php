<?php
error_reporting(0);
session_start();
require_once("db.php");
$auth_status = false;
if(isset($_SESSION['rep_auth'])) {
    $rep_id = $_SESSION['rep_id'];
    $sql = "SELECT email_host,email_username,email_password FROM salesrep WHERE rep_id='$rep_id' AND email_host IS NOT NULL";
    $result = mysqli_query($server_connection,$sql);
    while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        $auth_status = true;
        extract($row);
        $hostname = '{'.$email_host.'/imap/ssl/novalidate-cert}INBOX';
        $username = $email_username;
        $password = $email_password;
    }
} else if(isset($_SESSION['admin_auth'])) {
    $sql = "SELECT inbox_host as email_host,inbox_username,inbox_password FROM admin WHERE inbox_host IS NOT NULL";
    $result = mysqli_query($server_connection,$sql);
    while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        $auth_status = true;
        extract($row);
        $hostname = '{'.$email_host.'/imap/ssl/novalidate-cert}INBOX';
        $username = $inbox_username;
        $password = $inbox_password;
    }
}
if($auth_status) {

    //$inbox = imap_open($hostname,$username,$password) or die('Your email inbox is not setup yet. To setup your inbox please click on the settings link and enter in your email login settings
    //           to create your inbox. ATTN: You will only be able to see the emails that have an email address in the CRM. This means Sales Reps and
    //           Leads only can be viewed in this inbox. All other PERSONAL and SPAM will not show. This is done buy scanning matching CRM email
    //           addresses only from your account for display.');

    $inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

    $from = array();
    $sql = "SELECT email FROM wlobster WHERE email !='' ";
    $result = mysqli_query($server_connection,$sql);
    while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        array_push($from,$row['email']);
    }
    $emails = array();
//    foreach($from as $search) {
        $emails[] = imap_search($inbox, 'ALL');
//    }
    $emails = $emails[0];
    if($emails) {
        $output = '';
        rsort($emails);
    }
//    $n_msgs = imap_num_msg($inbox);
//    foreach ($n_msgs as $ssk) {
//        $header[] = $ssk;
//    }
//    print_r($header);

}
?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-envelope"></i>Inbox</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                    <thead>
                    <style>
                        th.sorting {
                            width: 340px!important;
                        }
                    </style>
                    <tr>
                        <th>Action</th>
                        <th>Name</th>
                        <th>Subject</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($auth_status) {
                        /* For each email... */
                        if($emails){

                            foreach($emails as $email_number) {
//print_r($email_number);
//                                $check = imap_mailboxmsginfo($email_number);
//                               print_r($check);
                                $headerInfo = imap_headerinfo($inbox,$email_number);
//                                print_r($headerInfo);
                                $structure = imap_fetchstructure($inbox, $email_number);

                                /* get information specific to this email */
                                $overview = imap_fetch_overview($inbox,$email_number,0);

                                /* get mesage body */
                                $message = imap_fetchbody($inbox,$email_number,1.1,FT_PEEK);
                                //$message = base64_decode($message);
                                if(strcmp($message,"")==0)
                                    $message = imap_fetchbody($inbox,$email_number,1,FT_PEEK);
                                $output .= 'Subject: '.$overview[0]->subject.'<br />';
                                $output .= 'Body: '.$message.'<br />';
                                $output .= 'From: '.$overview[0]->from.'<br />';
                                $output .= 'Date: '.$overview[0]->date.'<br />';
                                $output .= 'Seen: '.$overview[0]->seen.'<br />';
                                $is_attachment = false;
                                if(isset($structure->parts) && count($structure->parts))
                                {
                                    for($i = 0; $i < count($structure->parts); $i++)
                                    {
                                        $attachments[$i] = array(
                                            'is_attachment' => false,
                                            'filename' => '',
                                            'name' => '',
                                            'attachment' => ''
                                        );

                                        if($structure->parts[$i]->ifdparameters)
                                        {
                                            foreach($structure->parts[$i]->dparameters as $object)
                                            {
                                                if(strtolower($object->attribute) == 'filename')
                                                {
                                                    $is_attachment = true;
                                                }
                                            }
                                        }

                                        if($structure->parts[$i]->ifparameters)
                                        {
                                            foreach($structure->parts[$i]->parameters as $object)
                                            {
                                                if(strtolower($object->attribute) == 'name')
                                                {
                                                    $is_attachment = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                                <tr <?php if(!$overview[0]->seen) { ?>class="unread"<?php } ?> data-messageid="<?php echo $email_number; ?>">
                                    <td><div class="btn-group input-actions" style="position:relative;margin-right:10px;">
                                            <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="javascript:deleteEmailMessages();">
                                                        <i class="fa fa-trash-o"></i> Delete From Local </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:deleteEmailInbox();">
                                                        <i class="fa fa-trash-o"></i> Delete From Server </a>
                                                </li>
                                            </ul>
                                        </div></td>
                                    <td class="email_view-message">
                                        <input type="checkbox" name="delete" class="mail-checkbox" value="<?php echo  $overview[0]->msgno; ?>">
                                        <img style="border-radius: 50% !important;margin-right: 5px;width:40px;height:40px;" src="backend/upload/display_pictures/default.png">
                                        <?php echo $overview[0]->from; ?>
                                        <span class="badge badge-warning" style="margin-left:10px;">Customer</span>
                                    </td>
                                    <td class="email_view-message "> <?php echo $overview[0]->subject; ?>
                                        <?php if($is_attachment) { ?><i class="fa fa-paperclip" aria-hidden="true" style='padding-left: 10px;'></i><?php } ?></td>
                                    <td class="email_view-message text-right"> <?php echo $overview[0]->date; ?> </td>
                                </tr>
                                <?php
                                /* change the status */
                                // $status = imap_setflag_full($inbox, $overview[0]->msgno, "\Seen \Flagged");
                            }
                        } else {
                            ?>
                            <tr>
                                <td style="text-align:center;" colspan="3">No emails</td>
                                <td style="text-align:center;"></td>
                                <td style="text-align:center;"></td>
                            </tr>
                            <?php
                        }

                        imap_close($inbox);
                    } else {
                        ?>
                        <tr>
                            <td style="text-align:center;" colspan="3">Complete your Email Settings</td>
                            <td style="text-align:center;"></td>
                            <td style="text-align:center;"></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
<script>

</script>