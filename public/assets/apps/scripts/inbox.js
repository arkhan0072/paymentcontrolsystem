var AppInbox = function() {

    var content = $('.inbox-content');
    var listListing = '';

    var loadInbox = function(el, name, domain) {
        if (domain == "internal") {
            if (name == "inbox")
                var url = 'backend/chat_inbox.php';
            else if (name == "sent")
                var url = 'backend/chat_sent.php';
            else if (name == "trash")
                var url = 'backend/chat_trash.php';
        } else if (domain == "external") {
            if (name == "inbox")
                var url = 'backend/mail_inbox.php';
            else if (name == "sent")
                var url = 'backend/mail_sent.php';
            else if (name == "trash")
                var url = 'backend/mail_trash.php';
        }
        var title = el.attr('data-title');
        listListing = name;

        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) {
                toggleButton(el);

                App.unblockUI('.inbox-content');

                $('.inbox-nav > li.active').removeClass('active');
                el.closest('li').addClass('active');
                $('.inbox-header > h1').text(title);

                content.html(res);

                if (Layout.fixContentHeight) {
                    Layout.fixContentHeight();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });

        // handle group checkbox:
        jQuery('body').on('change', '.mail-group-checkbox', function() {
            var set = jQuery('.mail-checkbox');
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function() {
                $(this).attr("checked", checked);
            });
        });
    }

    var loadMessage = function(el, name, resetMenu) {
        var url = 'backend/chat_view.php';

        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        var message_id = el.parent('tr').attr("data-messageid");

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: { 'message_id': message_id },
            success: function(res) {
                App.unblockUI(content);

                toggleButton(el);

                if (resetMenu) {
                    $('.inbox-nav > li.active').removeClass('active');
                }
                $('.inbox-header > h1').text('View Message');

                content.html(res);
                Layout.fixContentHeight();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
    }

    var loadEmailMessage = function(el, name, resetMenu) {
        var url = 'backend/mail_view.php';

        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        var message_id = el.parent('tr').attr("data-messageid");

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: { 'message_id': message_id },
            success: function(res) {
                App.unblockUI(content);

                toggleButton(el);

                if (resetMenu) {
                    $('.inbox-nav > li.active').removeClass('active');
                }
                $('.inbox-header > h1').text('View Message');

                content.html(res);
                Layout.fixContentHeight();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
    }

    var loadEmailSent = function(el, name, resetMenu) {
        var url = 'backend/mail_view_sent.php';

        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        var message_id = el.parent('tr').attr("data-messageid");

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: { 'message_id': message_id },
            success: function(res) {
                App.unblockUI(content);

                toggleButton(el);

                if (resetMenu) {
                    $('.inbox-nav > li.active').removeClass('active');
                }
                $('.inbox-header > h1').text('View Message');

                content.html(res);
                Layout.fixContentHeight();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
    }

    var initWysihtml5 = function() {
        $('.inbox-wysihtml5').wysihtml5({
            "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
        });
    }

    var initFileupload = function() {

        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: '../assets/global/plugins/jquery-file-upload/server/php/',
            autoUpload: true
        });

        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '../assets/global/plugins/jquery-file-upload/server/php/',
                type: 'HEAD'
            }).fail(function() {
                $('<span class="alert alert-error"/>')
                    .text('Upload server currently unavailable - ' +
                        new Date())
                    .appendTo('#fileupload');
            });
        }
    }

    var loadCompose = function(el, domain) {
        if (domain == "internal")
            var url = 'backend/chat_compose.php';
        else if (domain == "external")
            var url = 'backend/mail_compose.php';
        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        // load the form via ajax
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) {
                App.unblockUI(content);
                toggleButton(el);

                $('.inbox-nav > li.active').removeClass('active');
                $('.inbox-header > h1').text('Compose');

                content.html(res);

                //initFileupload();
                initWysihtml5();

                $('.inbox-wysihtml5').focus();
                Layout.fixContentHeight();
                var url = location.search;
                if (url.includes("?to="))
                    $("#to").val(url.substr(url.indexOf("=") + 1));
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
    }

    var loadReply = function(el, domain) {
        var messageid = $(el).attr("data-messageid");
        if (domain == "internal") {
            var url = 'backend/chat_reply.php';
            var data = { 'message_id': messageid };
        } else if (domain == "external") {
            var messagefrom = $(el).attr("data-messagefrom");
            var messagesubject = $(el).attr("data-messagesubject");
            var message = $(el).attr("data-message");
            var url = 'backend/mail_reply.php';
            var data = { 'message_id': messageid, 'message_from': messagefrom, 'message_subject': messagesubject, 'message': message };
        }

        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        // load the form via ajax
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: data,
            success: function(res) {
                App.unblockUI(content);
                toggleButton(el);

                $('.inbox-nav > li.active').removeClass('active');
                $('.inbox-header > h1').text('Reply');

                content.html(res);
                $('[name="message"]').val($('#reply_email_content_body').html());

                handleCCInput(); // init "CC" input field

                //initFileupload();
                initWysihtml5();
                Layout.fixContentHeight();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
    }

    var loadSetting = function(el) {
        var url = 'backend/mail_settings.php';
        App.blockUI({
            target: content,
            overlayColor: 'none',
            animate: true
        });

        toggleButton(el);

        // load the form via ajax
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) {
                App.unblockUI(content);
                toggleButton(el);

                $('.inbox-nav > li.active').removeClass('active');
                $('.inbox-header > h1').text('Compose');

                content.html(res);

                //initFileupload();
                initWysihtml5();

                $('.inbox-wysihtml5').focus();
                Layout.fixContentHeight();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
    }

    var loadDelete = function(el, domain) {
        if (domain == "internal")
            var url = "backend/deleteMessage.php";
        else if (domain == "external")
            var url = "backend/deleteEmailMessage.php";
        else if (domain == "imap")
            var url = "backend/deleteImapMessage.php";
        var retVal = confirm("Are you sure to delete this message?");
        if (retVal == true) {
            var messageid = $(el).attr("data-messageid");
            url = url + "?message_id=" + messageid;
            $.get(url, function(data, status) {
                alert(data.message);
                if (data.status == "success") {
                    window.location.reload();
                }
            });
        } else {
            return false;
        }
    }

    var handleCCInput = function() {
        var the = $('.inbox-compose .mail-to .inbox-cc');
        var input = $('.inbox-compose .input-cc');
        the.hide();
        input.show();
        $('.close', input).click(function() {
            input.hide();
            the.show();
        });
    }

    var handleBCCInput = function() {

        var the = $('.inbox-compose .mail-to .inbox-bcc');
        var input = $('.inbox-compose .input-bcc');
        the.hide();
        input.show();
        $('.close', input).click(function() {
            input.hide();
            the.show();
        });
    }

    var toggleButton = function(el) {
        if (typeof el == 'undefined') {
            return;
        }
        if (el.attr("disabled")) {
            el.attr("disabled", false);
        } else {
            el.attr("disabled", true);
        }
    }

    return {
        //main function to initiate the module
        init: function() {
            var href = location.href;
            if (href.includes("email_inbox")) {
                var url = location.search;
                if (url.includes("?to=")) {
                    $(window).load(function() {
                        $(".email_compose-btn").click();
                    });
                }
            } else {
                var url = location.search;
                if (url.includes("?to=")) {
                    $(window).load(function() {
                        $(".compose-btn").click();
                    });
                }
            }

            // handle compose btn click
            $('.inbox').on('click', '.compose-btn', function() {
                loadCompose($(this), "internal");
            });
            $('.inbox').on('click', '.email_compose-btn', function() {
                loadCompose($(this), "external");
            });

            // handle discard btn
            $('.inbox').on('click', '.inbox-discard-btn', function(e) {
                e.preventDefault();
                loadInbox($(this), listListing, 'internal');
            });

            // handle reply and forward button click
            $('.inbox').on('click', '.reply-btn', function() {
                loadReply($(this), "internal");
            });
            $('.inbox').on('click', '.email_reply-btn', function() {
                loadReply($(this), "external");
            });

            // handle delete button click
            $('.inbox').on('click', '.delete-btn', function() {
                loadDelete($(this), "internal");
            });
            $('.inbox').on('click', '.delete-reply-btn', function() {
                loadDelete($(this), "external");
            });
            $('.inbox').on('click', '.delete-btn-mail', function() {
                loadDelete($(this), "imap");
            });

            // handle view message
            $('.inbox').on('click', '.view-message', function(e) {
                if (e.target.type == "checkbox") {
                    // stop the bubbling to prevent firing the row's click event
                    e.stopPropagation();
                } else {
                    loadMessage($(this));
                }
            });
            $('.inbox').on('click', '.email_view-message', function(e) {
                if (e.target.type == "checkbox") {
                    // stop the bubbling to prevent firing the row's click event
                    e.stopPropagation();
                } else {
                    loadEmailMessage($(this));
                }
            });
            $('.inbox').on('click', '.email_sent_view-message', function(e) {
                if (e.target.type == "checkbox") {
                    // stop the bubbling to prevent firing the row's click event
                    e.stopPropagation();
                } else {
                    loadEmailSent($(this));
                }
            });

            // handle inbox listing
            $('.inbox-nav > .inbox_li').click(function() {
                loadInbox($(this), 'inbox', 'internal');
            });

            // handle sent listing
            $('.inbox-nav > .sent_li').click(function() {
                loadInbox($(this), 'sent', 'internal');
            });

            // handle trash listing
            $('.inbox-nav > .trash_li').click(function() {
                loadInbox($(this), 'trash', 'internal');
            });
            $('.inbox-nav > .email_trash_li').click(function() {
                loadInbox($(this), 'trash', 'external');
            });

            $('.inbox-nav > .email_inbox_li').click(function() {
                loadInbox($(this), 'inbox', 'external');
            });
            $('.inbox-nav > .email_sent_li').click(function() {
                loadInbox($(this), 'sent', 'external');
            });

            $('.inbox-nav > .setting_li').click(function() {
                loadSetting($(this));
            });

            //handle compose/reply cc input toggle
            $('.inbox-content').on('click', '.mail-to .inbox-cc', function() {
                handleCCInput();
            });

            //handle compose/reply bcc input toggle
            $('.inbox-content').on('click', '.mail-to .inbox-bcc', function() {
                handleBCCInput();
            });

            //handle loading content based on URL parameter
            if (App.getURLParameter("a") === "view") {
                loadMessage();
            } else if (App.getURLParameter("a") === "compose") {
                loadCompose();
            } else {
                $('.inbox-nav > li:first > a').click();
            }

        }

    };

}();

jQuery(document).ready(function() {
    AppInbox.init();
});

function deleteImapMessages() {
    var ids = [];
    $.each($("input[name='delete']:checked"), function() {
        ids.push($(this).val());
    });
    var message_ids = ids.join(",");
    if (message_ids != '') {
        var url = "backend/deleteImapMessage.php";
        var retVal = confirm("Are you sure to delete these message?");
        if (retVal == true) {
            url = url + "?message_id=" + message_ids;
            $.get(url, function(data, status) {
                alert(data.message);
                if (data.status == "success") {
                    window.location.reload();
                }
            });
        } else {
            return false;
        }
    }
}

function deleteEmailMessages() {
    var ids = [];
    $.each($("input[name='delete']:checked"), function() {
        ids.push($(this).val());
    });
    var message_ids = ids.join(",");
    if (message_ids != '') {
        var url = "backend/deleteEmailMessage.php";
        var retVal = confirm("Are you sure to delete these message?");
        if (retVal == true) {
            url = url + "?message_id=" + message_ids;
            $.get(url, function(data, status) {
                alert(data.message);
                if (data.status == "success") {
                    window.location.reload();
                }
            });
        } else {
            return false;
        }
    }
}

function deleteMessages() {
    var ids = [];
    $.each($("input[name='delete']:checked"), function() {
        ids.push($(this).val());
    });
    var message_ids = ids.join(",");
    if (message_ids != '') {
        var url = "backend/deleteMessage.php";
        var retVal = confirm("Are you sure to delete these message?");
        if (retVal == true) {
            url = url + "?message_id=" + message_ids;
            $.get(url, function(data, status) {
                alert(data.message);
                if (data.status == "success") {
                    window.location.reload();
                }
            });
        } else {
            return false;
        }
    }
}