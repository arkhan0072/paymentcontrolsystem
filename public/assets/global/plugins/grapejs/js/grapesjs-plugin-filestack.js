/*! grapesjs-plugin-filestack - 0.0.2 */ ! function(e, t) { "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? exports["grapesjs-plugin-filestack"] = t() : e["grapesjs-plugin-filestack"] = t() }(this, function() {
    return function(e) {
        function t(o) { if (n[o]) return n[o].exports; var r = n[o] = { exports: {}, id: o, loaded: !1 }; return e[o].call(r.exports, r, r.exports, t), r.loaded = !0, r.exports }
        var n = {};
        return t.m = e, t.c = n, t.p = "", t(0)
    }([function(e, t) {
        "use strict";
        grapesjs.plugins.add("gjs-plugin-filestack", function(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                n = t,
                o = e.getConfig(),
                r = o.stylePrefix || "",
                i = void 0,
                s = { key: "", btnEl: "", btnText: "Add images", filestackOpts: { accept: "image/*", maxFiles: 10 }, onComplete: function(e, t) {} };
            for (var a in s) a in n || (n[a] = s[a]);
            if (!filestack) throw new Error("Filestack instance not found");
            if (!n.key) throw new Error("Filestack's API key not found");
            var l = filestack.init(n.key);
            e.on("run:open-assets", function() {
                var t = e.Modal,
                    o = t.getContentEl(),
                    s = o.querySelector("." + r + "am-file-uploader"),
                    a = o.querySelector("." + r + "am-assets-header"),
                    f = o.querySelector("." + r + "am-assets-cont");
                s && (s.style.display = "none"), a && (a.style.display = "none"), f.style.width = "100%", i || (i = n.btnEl, i || (i = document.createElement("button"), i.className = r + "btn-prim " + r + "btn-filestack", i.innerHTML = n.btnText), i.onclick = function() {
                    l.pick(n.filestackOpts).then(function(e) {
                        var t = e.filesUploaded,
                            o = t instanceof Array ? t : [t],
                            r = c(o);
                        n.onComplete(o, r)
                    })
                }), f.insertBefore(i, a)
            });
            var c = function(t) { var n = t.map(function(e) { return e.src = e.url, e }); return e.AssetManager.add(n) }
        })
    }])
});