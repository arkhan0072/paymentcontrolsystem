function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var ChartsAmcharts = function() 
{
    var initChartSample12 = function(data1,data2) 
    {
        var chartData1 = [];
		var chartData2 = [];
        generateChartData();

        function generateChartData() 
        {
            for (var i = 0; i < data1.length; i++) 
            {
                var newDate = new Date(data1[i].Date);
                newDate.setDate(newDate.getDate());

                //var a = Math.round(Math.random() * (40 + i)) + 100 + i;
                var a=data1[i].Count;
                chartData1.push({
                    date: newDate,
                    value: a
                    //volume: b
                });
            }
			for (var i = 0; i < data2.length; i++) 
            {
                var newDate = new Date(data2[i].Date);
                newDate.setDate(newDate.getDate());

                //var a = Math.round(Math.random() * (40 + i)) + 100 + i;
                var a=data2[i].Count;
                chartData2.push({
                    date: newDate,
                    value: a
                    //volume: b
                });
            }
        }
		var chart = AmCharts.makeChart( "chart_12", {
		  type: "stock",
            "theme": "light",
            pathToImages: "assets/global/plugins/" + "amcharts/amcharts/images/",
            "fontFamily": 'Open Sans',
            
            "color":    '#d41c5a',
		  "dataSets": [ {
			  "title": "Registration",
			  "fieldMappings": [ {
				"fromField": "value",
				"toField": "value"
			  }, {
				"fromField": "date",
				"toField": "date"
			  } ],
			  "dataProvider": chartData1,
			  "categoryField": "date"
			}, {
			  "title": "Verified",
			  "fieldMappings": [ {
				"fromField": "value",
				"toField": "value"
			  }, {
				"fromField": "date",
				"toField": "date"
			  } ],
			  "dataProvider": chartData2,
			  "categoryField": "date"
			}
		  ],

		  "panels": [ {
			"showCategoryAxis": false,
			"title": "Value",
			"percentHeight": 70,
			"stockGraphs": [ {
			  "id": "g1",
			  "valueField": "value",
			  "comparable": true,
			  "compareField": "value",
			  "balloonText": "[[title]]:<b>[[value]]</b>",
			  "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
			} ],
			"stockLegend": {
			  "periodValueTextComparing": "[[percents.value.close]]%",
			  "periodValueTextRegular": "[[value.close]]"
			}
		  }, {
			"title": "Volume",
			"percentHeight": 30,
			"stockGraphs": [ {
			  "valueField": "value",
			  "type": "column",
			  "showBalloon": true,
			  "fillAlphas": 1
			} ],
			"stockLegend": {
			  "periodValueTextRegular": "[[value.close]]"
			}
		  } ],

		  "chartScrollbarSettings": {
			"graph": "g1"
		  },

		  "chartCursorSettings": {
			"valueBalloonsEnabled": true,
			"fullWidth": true,
			"cursorAlpha": 0.1,
			"valueLineBalloonEnabled": true,
			"valueLineEnabled": true,
			"valueLineAlpha": 0.5
		  },

		  periodSelector: 
			{
				periods: [ 
				{
				  period: "DD",
				  count: 10,
				  label: "10 days"
				}, 
				{
				  period: "MM",
				  count: 1,
				  label: "1 month"
				},
				{
				  period: "YTD",
				  label: "YTD"
				},
				{
				  period: "MAX",
				  label: "MAX"
				} ]
			},

		  "dataSetSelector": {
			"position": "top"
		  },

		  "export": {
			"enabled": true
		  }
		});

        $('#chart_12').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
    
    var initChartSample13 = function(response) 
    {
        var chartData = [];
		var eventPoints=[];
        generateChartData();
		
        function generateChartData() 
        {
			var data=response.data;
            for (var i = 0; i < data.length; i++) 
            {
                var newDate = new Date(data[i].Date);
                newDate.setDate(newDate.getDate());

                //var a = Math.round(Math.random() * (40 + i)) + 100 + i;
                var a=data[i].Count;
                chartData.push({
                    date: newDate,
                    value: a
                });
            }
			var data=response.events;
			for (var i = 0; i < data.length; i++) 
            {
				var t=data[i];
                var newDate = new Date(t.date);
                newDate.setDate(newDate.getDate());
				newDate.setMonth(newDate.getMonth());
				t.date=newDate;
				eventPoints.push(t);
            }
        }
		
        var chart = AmCharts.makeChart("chart_13", {
            type: "stock",
            "theme": "light",
            pathToImages: "assets/global/plugins/" + "amcharts/amcharts/images/",
            "fontFamily": 'Open Sans',
            
            "color":    '#888',
            dataSets: [
            {
                color: "#b0de09",
                fieldMappings: [{
                    fromField: "value",
                    toField: "value"
                }],
                dataProvider: chartData,
                categoryField: "date",
				stockEvents: eventPoints,

            }],

            panels: [{
                title: "Unimatch",
                percentHeight: 70,

                stockGraphs: [{
                    id: "g1",
                    valueField: "value"
                }],

                stockLegend: {
                     markerType: "none",
					valueTextRegular: ""
                }
            }],
			periodSelector: 
			{
				periods: [ 
				{
				  period: "DD",
				  count: 10,
				  label: "10 days"
				}, 
				{
				  period: "MM",
				  count: 1,
				  label: "1 month"
				},
				{
				  period: "MAX",
				  label: "MAX"
				} ]
			},

            chartScrollbarSettings: {
                graph: "g1"
            },

		chartCursorSettings: 
		{
			"valueBalloonsEnabled": true,
			"graphBulletSize": 1,
			"valueLineBalloonEnabled": true,
			"valueLineEnabled": true,
			"valueLineAlpha": 0.5
		},


            panelsSettings: {
                usePrefixes: true
            },
			"export": {
				"enabled": true
			}
        });

        $('#chart_13').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
	var initChartSample14 = function(response)
	{
		var chartData=response;
		var chart = AmCharts.makeChart("chart_14", {
		"theme": "light",
		"type": "serial",
		"startDuration": 2,
		"dataProvider": chartData,
		"valueAxes": [{
			"position": "left",
			"axisAlpha":0,
			"gridAlpha":0
		}],
		"graphs": [{
			"balloonText": "[[category]]: <b>[[value]]</b>",
			"colorField": "Color",
			"fillAlphas": 0.85,
			"lineAlpha": 0.1,
			"type": "column",
			"topRadius":0.2,
			"valueField": "Count"
		}],
		"depth3D": 30,
		"angle": 30,
		"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
		},
		"categoryField": "Title",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha":0,
			"gridAlpha":0

		},
		"export": {
			"enabled": true
		 }

	}, 0);
		
	}

    return {
        //main function to initiate the module
	
        init: function() 
        {
			//initChartSample10(null);
			$.getJSON( "charts/dashboard.php", function(data)
			{
				//initChartSample10(data.coordinates);
				initChartSample12(data.registration,data.verified);
				initChartSample13(data.unimatch);
				initChartSample14(data.statistics);
				/*initChartSample5(data.page.web);
                initChartSample50(data.page.app);*/
            });
        }

    };

}();
