function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var ChartsAmcharts = function() 
{
	
	 var initChartSample5 = function(data) 
	 {
		var dataPoints=[];
		function getRandomColor() 
		{
			var letters = '0123456789ABCDEF';
			var color = '#';
			for (var i = 0; i < 6; i++ ) 
			{
				color += letters[Math.floor(Math.random() * 16)];
			}
			return color;
		}
		for(var i=0;i<data.length;i++)
		{
			dataPoints.push({"page":Object.keys(data[i])[0],"visits":Object.values(data[i])[0],"color":getRandomColor()});
		}
		
        var chart = AmCharts.makeChart("chart_5", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": dataPoints,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "page",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "labelsEnabled": false

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

        jQuery('.chart_5_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            chart.startDuration = 0;

            if (property == 'topRadius') {
                target = chart.graphs[0];
            }

            target[property] = this.value;
            chart.validateNow();
        });

        $('#chart_5').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSample50 = function(data) 
     {
        var dataPoints=[];
        function getRandomColor() 
        {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++ ) 
            {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        for(var i=0;i<data.length;i++)
        {
            dataPoints.push({"page":Object.keys(data[i])[0],"visits":Object.values(data[i])[0],"color":getRandomColor()});
        }
        
        var chart = AmCharts.makeChart("chart_50", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": dataPoints,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "page",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "labelsEnabled": false

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

        jQuery('.chart_50_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            chart.startDuration = 0;

            if (property == 'topRadius') {
                target = chart.graphs[0];
            }

            target[property] = this.value;
            chart.validateNow();
        });

        $('#chart_50').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


 var initChartSample10 = function(data) {
        /*
            although ammap has methos like getAreaCenterLatitude and getAreaCenterLongitude,
            they are not suitable in quite a lot of cases as the center of some countries
            is even outside the country itself (like US, because of Alaska and Hawaii)
            That's why wehave the coordinates stored here
        */
        var latlong = {};
        var mapData = [];
		for(var i=0;i<data.length;i++)
		{
			var temp=data[i].coordinates;
			temp=temp.split(",");
			latlong[data[i].city]={
			"latitude": temp[0],
            "longitude": temp[1]
			};
			mapData.push({"code":data[i].city,"name":data[i].city,"value":data[i].count,"color": "#eea638"});
		}
		
       // var latlong = {};

        /*var mapData = [{
            "code": "AF",
            "name": "Afghanistan",
            "value": 32358260,
            "color": "#eea638"
        }];*/

        var map;
        var minBulletSize = 3;
        var maxBulletSize = 70;
        var min = Infinity;
        var max = -Infinity;


        // get min and max values
        // build map
        buildMap() ;
        function buildMap() 
        {
            AmCharts.theme = AmCharts.themes.dark;
            map = new AmCharts.AmMap();
            map.pathToImages = "assets/global/plugins/" + "amcharts/ammap/images/",

            map.fontFamily = 'Open Sans';
            map.fontSize = '13';
            map.color = '#888';
            
            //map.addTitle("Population of the World in 2011", 14);
            map.addTitle("Meritlane Page Visits", 11);
            map.areasSettings = {
                unlistedAreasColor: "#000000",
                unlistedAreasAlpha: 0.1
            };
            map.imagesSettings.balloonText = "<span style='font-size:14px;'><b>[[title]]</b>: [[value]]</span>";

            var dataProvider = {
                mapVar: AmCharts.maps.worldLow,
                images: []
            }

            // create circle for each country
            for (var i = 0; i < mapData.length; i++) 
            {
                var dataItem = mapData[i];
                var value = dataItem.value;
                var size=8;
                var id = dataItem.code;

                dataProvider.images.push({
                    type: "circle",
                    width: size,
                    height: size,
                    color: dataItem.color,
                    longitude: latlong[id].longitude,
                    latitude: latlong[id].latitude,
                    title: dataItem.name,
                    value: value
                });
            }

            map.dataProvider = dataProvider;
            map.write("chart_10");
        };

        $('#chart_10').closest('.portlet').find('.fullscreen').click(function() {
            map.invalidateSize();
        });
    }
    


    var initChartSample12 = function(data) 
    {
        var chartData = [];
        generateChartData();

        function generateChartData() 
        {
            var firstDate = new Date(2012, 0, 1);
            firstDate.setDate(firstDate.getDate() - 500);
            firstDate.setHours(0, 0, 0, 0);
            for (var i = 0; i < data.length; i++) 
            {
                var newDate = new Date(data[i].Visit_Time);
                newDate.setDate(newDate.getDate());

                //var a = Math.round(Math.random() * (40 + i)) + 100 + i;
                var a=data[i].count;
                var b = Math.round(Math.random() * 100000000);
				
                chartData.push({
                    date: newDate,
                    value: a,
                    volume: b
                });
            }
        }

        var chart = AmCharts.makeChart("chart_12", {
            type: "stock",
            "theme": "light",
            pathToImages: "assets/global/plugins/" + "amcharts/amcharts/images/",
            "fontFamily": 'Open Sans',
            
            "color":    '#888',
            dataSets: [
            {
				title:"Web",
                color: "#b0de09",
                fieldMappings: [{
                    fromField: "value",
                    toField: "value"
                }, {
                    fromField: "volume",
                    toField: "volume"
                }],
                dataProvider: chartData,
                categoryField: "date",
            },
            {
				title:"app",
                color: "#A0de09",
                fieldMappings: [{
                    fromField: "value",
                    toField: "value"
                }, {
                    fromField: "volume",
                    toField: "volume"
                }],
                dataProvider: chartData,
                categoryField: "date",
            }
            ],


            panels: [{
                title: "PageVisits",
                percentHeight: 70,

                stockGraphs: [{
                    id: "g1",
                    valueField: "value",
					comparedGraphLineThickness: 2,
					columnWidth: 0.7,
					useDataSetColors: false,
					comparable: true,
					compareField: "close",
					showBalloon: false,
					proCandlesticks: true
                }],

                stockLegend: {
                     markerType: "none",
					markerSize: 0,
					labelText: "",
					periodValueTextRegular: "[[value.close]]"
                }
            }],

            chartScrollbarSettings: {
                graph: "g1"
            },
		/*valueAxesSettings: 
		{
			gridColor: "#555",
			gridAlpha: 1,
			inside: false,
			showLastLabel: true
		},*/

		chartCursorSettings: 
		{
			pan: true,
			valueLineEnabled: true,
			valueLineBalloonEnabled: true,
			graphBulletSize: 1,
			valueLineAlpha:0.5
		},


            panelsSettings: {
                usePrefixes: true
            }
        });

        $('#chart_12').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
    
    

    return {
        //main function to initiate the module
	
        init: function() 
        {
			//initChartSample10(null);
			var query=getUrlVars();
			var q=query["type"];
			$.getJSON( "pagevisits/pagevisits.php?type="+q, function(data)
			{
				initChartSample10(data.coordinates);
				initChartSample12(data.historicdata);
				initChartSample5(data.page.web);
                initChartSample50(data.page.app);
            });
        }

    };

}();
