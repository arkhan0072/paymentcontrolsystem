var ComponentsCodeEditors = function() {


    var handleDemo4 = function() {
        var myTextArea = document.getElementById('code_editor_demo_4');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: false,
            matchBrackets: true,
            styleActiveLine: true,
            theme: "neat",
            mode: 'mixedhtml',
            readOnly: true
        });
    }

    var handleDemo5 = function() {
        var myTextArea = document.getElementById('code_editor_demo_5');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: false,
            matchBrackets: true,
            styleActiveLine: true,
            theme: "neat",
            mode: 'mixedhtml',
            readOnly: true
        });
    }

    var handleDemo6 = function() {
        var myTextArea = document.getElementById('code_editor_demo_6');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: false,
            matchBrackets: true,
            styleActiveLine: true,
            theme: "neat",
            mode: 'mixedhtml',
            readOnly: true
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo4();
            handleDemo5();
            handleDemo6();
            $("#manual_content").show();
            $("#automatic_content").hide();
        }
    };

}();

jQuery(document).ready(function() {
    ComponentsCodeEditors.init();
});