<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'OrlandoController@index')->name('home');

Route::post('/import', 'OrlandoController@import')->name('import');
Route::get('paymentdata', 'OrlandoController@paymentdata')->name('paymentdata');
Route::get('/logs', 'LogController@index')->name('logs');
Route::get('/getlogs', 'LogController@getlogs')->name('getlogs');
Route::get('/orland', 'OrlandoController@edit')->name('editOrlando');
Route::post('/updateOrlando', 'OrlandoController@update')->name('updateOrlando');

