<?php

namespace App\Http\Controllers;

use App\Log;
use App\Orlando;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
//use Excel;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;

class OrlandoController extends Controller
{

    public function import(Request $request)
    {
        $orlando = new Orlando();
        if ($request->hasFile('file')) {
            //add file to server get file name and date and extension
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            // Get file mime type
            $mimeType = $request->file('file')->getClientMimeType();
            // Get just name of image
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get extention of image
            $extension = $request->file('file')->getClientOriginalExtension();
            // Filename to store the image
//            $time = Carbon::now();
//            $time->toDateString();
            $number = uniqid();
            $fileNameToStore = $fileName . '-' . $number . '.' . $extension;
            // Upload image

            Storage::disk('files')->put($fileNameToStore, \File::get($request->file('file')));

            $tmpName = $_FILES['file']['tmp_name'];
            $csvAsArray = new \SplFileObject($tmpName);
            $csvAsArray->setFlags(\SplFileObject::READ_CSV);
            $re = array();
            $reports= '';
            $index = 1;
            $result = [];
            $start = 0;
            $d1 = date('m-d-Y h:i:s a', time());

            $d = str_replace(" ", "_", $d1);
            $d = str_replace(":", "_", $d1);
            $file_content='';
            Storage::disk('details')->put('importreport_for_'.$d.'.txt', $file_content);

            $count = 0;
            $sampledata = [];
            foreach ($csvAsArray as $csvLine) {
                if ($this->testingBefore($index, $csvLine)) {
                    $orlando = Orlando::create(
                        [
                            'client_name' => str_replace("'", " ", $csvLine[0]),
                            'date' => $csvLine[1],
                            'type' => $csvLine[2],
                            'total' => (double)str_replace(",", "", $csvLine[3]),
                            'note' => preg_replace('/[^0-9a-zA-Z ]/', '', $csvLine[4]),
                            'check_' => $csvLine[5],
                            'invoice' => $csvLine[6],
                            'method' => $csvLine[7],
                            'transaction_id' => $csvLine[8],
                            'employee' => NULL,
                            'received' => 'N',
                            'notes' => count($csvLine) > 10 ? $csvLine[11] : ""
                        ]);
                    $note = count($csvLine) > 10 ? $csvLine[11] : "";
                    $reports .= $index." ".$csvLine[0]." ".$csvLine[1]." ".$csvLine[2]." ".(double)str_replace(",", "", $csvLine[3])." ".$note." ".$csvLine[5]." ".$csvLine[6]." ".$csvLine[7]."".PHP_EOL;
                $count++;
                }else{
                    echo "false";
                }

                $index++;
            }
            $this->importReport($d, $reports);

        }

        if($count < 1) {
            Storage::disk('details')->put('importreport_for_'.$d.'.txt', "No data was imported");
        }
            Storage::disk('details')->put('lastimport.txt', $d1);


        return redirect()->back()->with('message', 'Data imported Successfuly!');

    }

    function importReport($d, $reports) {
    $myfile =   Storage::disk('details')->put('importreport_for_'.$d.'.txt', "$reports") or die("Unable to open file!");
        $log = new Log();
        $log->file_name = 'importreport_for_'.$d.'.txt';
        $log->date = Carbon::now();
        $log->created_at = Carbon::now();
        $log->updated_at = Carbon::now();
        $log->save();

    }
    function testingBefore($index, $csvLine)
    {
        return $index > 11
            && count($csvLine) > 6
            && strtoupper($csvLine[2]) == "PAYMENT"
            && (strtoupper($csvLine[7]) == "CHECK" OR strtoupper($csvLine[7]) == "CASH" OR strtoupper($csvLine[7]) == "OTHER" OR strtoupper($csvLine[7]) == "CREDIT CARD" OR strtoupper($csvLine[7]) == "JOBBER PAYMENTS")
            && $csvLine[0] != "Report totals:"
            && !is_null($csvLine[6])
            && empty(Orlando::where('invoice', $csvLine[6])->first());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function paymentdata(Request $request)
    {
        $log = Log::orderBy('id', 'DESC')->limit(1)->get();
        $data = Orlando::where('id', '>', 0)
            ->when($request->methodz, function ($q) use ($request) {
                return $q->where('method', $request->methodz);
            })
            ->when($request->payments, function ($q) use ($request) {
                return $q->where('received', $request->payments);
            })
            ->when($request->datepick, function ($q) use ($request) {
                $dates = explode(' - ', $request->datepick);
                $cDate = [$dates[0], $dates[1]];
                $q->whereBetween('orlandos.date', $cDate);
            });

        $collectedBetween = Orlando::where('total', '>', 0)
            ->when($request->methodz, function ($q) use ($request) {
            return $q->where('method', $request->methodz);
        })

            ->when($request->payments, function ($q) use ($request) {
                return $q->where('received', $request->payments);
            })
            ->when($request->datepick, function ($q) use ($request) {
                $dates = explode(' - ', $request->datepick);
                $cDate = [$dates[0], $dates[1]];
               return $q->whereBetween('orlandos.date', $cDate);
            })
            ->sum('total');


        $uncollectedBetween = Orlando::where('total', '<', 0)
            ->where('id', '>', 0) ->when($request->methodz, function ($q) use ($request) {
            return $q->where('method', $request->methodz);
        })
            ->when($request->payments, function ($q) use ($request) {
                return $q->where('received', $request->payments);
            })
            ->when($request->datepick, function ($q) use ($request) {
                $dates = explode(' - ', $request->datepick);
                $cDate = [$dates[0], $dates[1]];
                return $q->whereBetween('orlandos.date', $cDate);
            })

            ->sum('total');
        $unclollectdCash = Orlando::where('total', '<', 0)
            ->where('method', 'cash')
            ->sum('total');
        $unclollectdcheck = Orlando::where('total', '<', 0)
            ->where('method', 'check')
            ->sum('total');

        return DataTables::of($data)
            ->with('collectedBetween', $collectedBetween)
            ->with('totaluncolletedbetween', $uncollectedBetween)
            ->with('totaluncolletedCash', $unclollectdCash)
            ->with('totaluncolletedcheck', $unclollectdcheck)
            ->with('logs', $log[0]->date)
            ->addColumn('edit', function (Orlando $orlando){
                return '<a href="#">
                        <button class="btn btn-primary" data-id="'.$orlando->id.'" title="Edit" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button></a>';
            })
            ->rawColumns(['edit'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Orlando $orlando
     * @return \Illuminate\Http\Response
     */
    public function show(Orlando $orlando)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Orlando $orlando
     * @return \Illuminate\Http\Response
     */
    public function edit(Orlando $orlando,Request $request)
    {
        $data=Orlando::where('id',$request->id)->first();
        return  response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Orlando $orlando
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orlando $orlando)
    {
      return   Orlando::where('id', $request->id)
            ->update( [
                'client_name' => $request->client_name,
                'date' => $request->date,
                'type' => $request->type,
                'total' => $request->total,
                'note' => $request->note,
                'check_' => $request->check,
                'invoice' => $request->invoice,
                'method' => $request->methods,
                'transaction_id' => $request->transaction,
                'employee' => $request->employee,
                'received' => $request->received,
                'notes' => $request->notes
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Orlando $orlando
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orlando $orlando)
    {
        //
    }
}
